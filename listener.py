import os
from sys import platform as _platform
import optparse
import time
import pprint

from flask import Flask  
from flask import request  
app = Flask(__name__)

#---------------------
start = int(round(time.time()))

@app.route("/")
def hello_world():

    return "This is a BitBucket webhook processor. Contact Tony Wildish for more information."

def displayHTML(request):
    return 'Webhook server online! Go to <a href="https://bitbucket.com">Bitbucket</a> to configure your repository webhook for <a href="%s/webhook">%s/webhook</a>' % (request.url_root,request.url_root)

@app.route('/', methods=['GET'])
def index():  
    return displayHTML(request)

@app.route('/webhook', methods=['GET', 'POST'])
def tracking():  
    if request.method == 'POST':

        data = request.get_json()
        pp = pprint.PrettyPrinter( indent=2 )

        output = "/data/hook." + str( int( time.time() ) ) + ".json"
        print( "Writing JSON to ", output )
        f = open( output, mode="w" )
        f.write( pp.pformat( data ) )
        f.close()

        commit_author = data['actor']['username']
        commit_hash = data['push']['changes'][0]['new']['target']['hash'][:7]
        commit_url = data['push']['changes'][0]['new']['target']['links']['html']['href']

        print 'Webhook received! %s committed %s' % (commit_author, commit_hash)
        return 'OK'
    else:
        return displayHTML(request)

if __name__ == '__main__':
    parser = optparse.OptionParser(usage="python listener.py -p ")
    parser.add_option('-p', '--port', action='store', dest='port', help='The port to listen on.')
    (args, _) = parser.parse_args()
    if args.port == None:
        print "Missing required argument: -p/--port"
        sys.exit(1)
    app.run(host='0.0.0.0', port=int(args.port), debug=True)
