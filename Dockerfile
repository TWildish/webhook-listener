FROM python:2.7.14-alpine

COPY REQUIREMENTS.txt /tmp
RUN pip install -r /tmp/REQUIREMENTS.txt

RUN addgroup -g 124 genome
USER root:genome

COPY listener.py /app/listener.py

CMD [ "python", "/app/listener.py", "-p", "8009" ]

#
# docker build -t tonywildish/bbwebhook .
# docker push tonywildish/bbwebhook
